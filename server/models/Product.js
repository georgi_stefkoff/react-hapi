const mongoose = require('mongoose');

const ProductSchema = new mongoose.Schema({
    title: String,
    code: String
});

ProductSchema.methods.delete = (code) => {
    return this.model.remove();
}

ProductSchema.methods.update = (newTitle) => {
    this.title = newTitle;

    return this.save();
}

module.exports = mongoose.model('Products', ProductSchema, 'Products');