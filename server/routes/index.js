const user = require('./user.routes');
const product = require('./product.routes');

module.exports = [
    ...user,
    ...product
];