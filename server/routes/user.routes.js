const userController = require('../controllers/user.controller');
const Joi = require('joi');

module.exports = [
    {
        method: 'GET',
        path: '/me',
        config: {
            auth: 'session',
            handler: userController.me
        }
    },
    {
        method: 'GET',
        path: '/login',
        config: {
            auth: {
                mode: 'try',
                strategy: 'session'
            },
            validate: {
                query: {
                    username: Joi.string().min(3).required(),
                    password: Joi.string().min(3).required()
                }
            },
            handler: userController.login,
        }
    },
    {
        method: 'GET',
        path: '/logout',
        config: {
            auth: 'session',
            handler: userController.logout
        }
    },
    {
        method: 'POST',
        path: '/register',
        config: {
            auth: false,
            validate: {
                payload: {
                    username: Joi.string().min(3).required(),
                    password: Joi.string().min(6).required(),
                    password_repeat: Joi.string().min(6).required(),
                    email: Joi.string().email().required()
                }
            },
            handler: userController.register
        }
    }
];