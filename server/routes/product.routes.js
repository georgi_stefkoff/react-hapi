const productController = require('../controllers/product.controller');
const Joi = require('joi');

module.exports = [
    {
        method: 'GET',
        path: '/product/{id}',
        config: {
            auth: 'session',
            handler: productController.get,
            validate: {
                params: {
                    id: Joi.string().required()
                }
            }
        }
    },
    {
        method: 'PUT',
        path: '/product/{id}',
        config: {
            auth: 'session',
            handler: productController.edit,
            validate: {
                params: {
                    id: Joi.string().required()
                }
            }
        }
    },
    {
        method: 'DELETE',
        path: '/product/{id}',
        config: {
            auth: 'session',
            handler: productController.delete,
            validate: {
                params: {
                    id: Joi.string().required()
                }
            }
        }
    },
    {
        method: 'POST',
        path: '/product',
        config: {
            auth: 'session',
            handler: productController.create,
            validate: {
                payload: {
                    title: Joi.string().min(6),
                    code: Joi.string().min(3)
                }
            }
        }
    },
    {
        method: 'GET',
        path: '/products',
        config: {
            auth: 'session',
            handler: productController.all
        }
    }
];