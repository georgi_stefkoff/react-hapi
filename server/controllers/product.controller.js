const ProductModel = require('../models/Product');
const Boom = require('boom');

module.exports = {
    create: async (req, h) => {
        const title = req.payload.title;
        const code = req.payload.code;

        const existingProduct = await ProductModel.findOne({
            code
        });

        if(existingProduct !== null){
            return Boom.badRequest('Product already exists');
        }

        const productModel = new ProductModel();
        productModel.title = title;
        productModel.code = code;

        const success = await !!productModel.save();

        return {
            success
        };
    },

    get: async (req, h) => {        
        const code = req.params.id;
        const productModel = await ProductModel.findOne({
            code
        });

        return productModel;
    },

    delete: async(req, h) => {
        const code = req.params.id;

        const productDeletion = await ProductModel.deleteOne({
            code
        });

        return {
            success: !!(productDeletion['n'] && productDeletion.n > 0)
        };
    },

    edit: async (req,h) => {
        const code = req.params.id;
        const newTitle = req.payload.title;

        const productModel = await ProductModel.findOne({
            code
        });

        if(!productModel){
            return Boom.badRequest('Product not found');
        }

        productModel.title = newTitle;

        const success = await !!productModel.save();

        return {
            success
        };
    },

    all: async () => {
        const products = await ProductModel.find();

        return products;
    }
}