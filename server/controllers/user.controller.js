const UserModel = require('../models/User');
const Boom = require('boom');
const Bcrypt = require('bcrypt');

module.exports = {
    login: async (request, h) => {
        if (request.auth.isAuthenticated) {
            return Boom.badRequest('User already logged in');
        }

        let account = null;
        const userModel = await UserModel.findOne({
            username: request.query.username
        }).exec();

        if(!userModel){
            return Boom.forbidden('User not found');
        }

        if(!await Bcrypt.compare(request.query.password, userModel.password)){
            return Boom.forbidden('Wrong credentials provided');
        }

        const sid = String(userModel._id);
        await request.server.app.cache.set(sid, {userModel}, 0);
        request.cookieAuth.set({sid});

        return {
            success: true
        }
    },

    register: async (req, h) => {
        const username = req.payload.username;
        const password = req.payload.password;
        const passwordRepeat = req.payload.password_repeat;
        const email = req.payload.email;

        if(password !== passwordRepeat){
            return Boom.badRequest('Passwords not match');
        }

        const existingUser = await UserModel.findOne({
            $or: [
                { username },
                { email }
            ]
        });

        if (existingUser !== null) {
            return Boom.badRequest('User already exists');
        }

        const userModel = new UserModel();
        userModel.username = username;
        userModel.password = await Bcrypt.hash(password, 10);
        userModel.email = email;

        const success = await !!userModel.save();

        return {
            success
        };
    },

    logout: (req, h) => {
        req.cookieAuth.clear();
        
        return {
            success: true
        };
    },

    me: async (req, h) => {
        const userModel = await UserModel.findById(req.auth.credentials.sid);
        
        return {
            username: userModel.username,
            email: userModel.email,
            id: userModel._id
        }
    }
};