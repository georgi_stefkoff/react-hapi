const Hapi = require('hapi');
const Mongoose = require('mongoose');

const serverConfig = require('./config/server');
const routes = require('./routes');
const cookieConfig = require('./config/cookie');
const dbConfig = require('./config/db');
const UserModel = require('./models/User');

const server = Hapi.server(serverConfig);
module.exports = {
    start: async () => {

        Mongoose.connect(dbConfig.host, { useNewUrlParser: true });
        Mongoose.connection.on('error', console.error);
        Mongoose.connection.on('open', () => console.log('Connection to MongoDb opened'));

        await server.register(require('hapi-auth-cookie'));

        const cache = server.cache({
            segment: 'sessions',
            expiresIn: 3 * 24 * 60 * 60 * 1000
        });
        server.app.cache = cache;

        server.auth.strategy('session', 'cookie', cookieConfig);
        server.auth.default('session');

        server.route(routes);

        await server.start();
        console.log('Server is running');
    }
};