module.exports = {
    password: 'someverysecurepasswordthatshouldbeminimum32characters',
    cookie: 'session',
    isSecure: false,
    ttl: 24 * 60 * 60 * 1000
};