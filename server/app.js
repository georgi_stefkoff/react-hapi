const server = require('./server');

server.start();
process.on('unhandledRejection', (err) => {
    console.log(err);
    process.exit();
});